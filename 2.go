package main

import (
	"fmt"
	"os"
	"path/filepath"
	"sync"
)

type DirStats struct {
	files       int
	directories int
	path        string
}

func countFilesAndDirs(root string, wg *sync.WaitGroup, statsCh chan DirStats) {
	defer wg.Done()

	files, err := os.ReadDir(root)
	if err != nil {
		fmt.Printf("Error reading directory %s: %s\n", root, err)
		return
	}

	dirStats := DirStats{0, 0, root}

	for _, file := range files {
		if file.IsDir() {
			dirStats.directories++
			wg.Add(1)
			go countFilesAndDirs(filepath.Join(root, file.Name()), wg, statsCh)
		} else {
			dirStats.files++
		}
	}

	statsCh <- dirStats
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Example: go run main.go <directory path>")
		return
	}

	rootDir := os.Args[1]
	var wg sync.WaitGroup
	statsCh := make(chan DirStats)

	wg.Add(1)
	go countFilesAndDirs(rootDir, &wg, statsCh)

	go func() {
		wg.Wait()
		close(statsCh)
	}()

	totalFiles := 0
	totalDirs := 0

	for stats := range statsCh {
		totalFiles += stats.files
		totalDirs += stats.directories

		fmt.Printf("Directory %s: %d files, %d directories\n", stats.path, stats.files, stats.directories)
	}

	fmt.Printf("Total: %d files, %d directories\n", totalFiles, totalDirs)
}
