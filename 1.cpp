#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

using namespace std;

// Global variables
int buffer[10];
int head = 0;
int tail = 0;
mutex m;
condition_variable cv;

// Producer thread function
void producer() {
  for (int i = 0; i < 10; i++) {
    // Acquire the lock
    lock_guard<mutex> lock(m);

    // Generate a random number
    int number = rand() % 100;
    cout << "Produced: " << number << endl;
    // Append the number to the buffer
    buffer[tail++] = number;

    // Notify the consumer thread that there is data available
    cv.notify_one();

    // Sleep for a random amount of time
    this_thread::sleep_for(chrono::milliseconds(rand() % 1000));
  }
}

// Consumer thread function
void consumer() {
  while (true) {
    // Wait until there is data available
    unique_lock<mutex> lock(m);
    cv.wait(lock, [&]() { return tail > head; });

    // Remove the number from the buffer
    int number = buffer[head++];

    // Print the number
    cout << "Consumed: " << number << endl;

    // Unlock the lock
    lock.unlock();
  }
}

int main() {
  // Create the producer and consumer threads
  thread producer_thread(producer);
  thread consumer_thread(consumer);

  // Join the threads
  producer_thread.join();
  consumer_thread.join();

  return 0;
}
