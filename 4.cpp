#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#define BUFFER_SIZE 10
typedef struct node {
  int data;
  struct node *next;
} Node;
Node *head = NULL, *tail = NULL;
int count = 0;
sem_t full, empty;
pthread_mutex_t lock;
void add_data(int data) {
  Node *new_node = (Node *)malloc(sizeof(Node));
  new_node->data = data;
  new_node->next = NULL;
  if (head == NULL) {  // corrected condition to check `head` instead of `tail`
    head = tail = new_node;
  } else {
    tail->next = new_node;
    tail = new_node;
  }
  count++;
}
int remove_data() {
  Node *temp = head;
  int data = temp->data;
  head = head->next;
  count--;

  // remove_data() function does not free the memory allocated for the node that
  // is being removed. This can cause a memory leak.
  free(temp);  // added free function call here
  return data;
}
void *producer(void *arg) {
  int i, data;
  for (i = 0; i < 100; i++) {
    data = rand() % 100;
    sem_wait(&empty);
    pthread_mutex_lock(&lock);
    add_data(data);
    printf("Produced: %d\n", data);
    pthread_mutex_unlock(&lock);
    sem_post(&full);
  }
}
void *consumer(void *arg) {
  int i, data;
  for (i = 0; i < 100; i++) {
    sem_wait(&full);
    pthread_mutex_lock(&lock);
    data = remove_data();
    printf("Consumed: %d\n", data);
    pthread_mutex_unlock(&lock);

    // Added semaphore wait here because ff you don't add the
    // semaphore wait  in the consumer
    // function, the consumer may consume data faster than
    // the producer can produce it. Resulting into SEGFAULT
    // error caused by the remove_data function
    sem_wait(&empty);

    sem_post(&empty);
  }
}
int main() {
  pthread_t producer_thread, consumer_thread;
  sem_init(&full, 0, 0);
  sem_init(&empty, 0, BUFFER_SIZE);
  pthread_mutex_init(&lock, NULL);
  pthread_create(&producer_thread, NULL, producer, NULL);
  pthread_create(&consumer_thread, NULL, consumer, NULL);
  pthread_join(producer_thread, NULL);
  pthread_join(consumer_thread, NULL);
  return 0;
}